package com.demo.springboot.domain.dto;

public class SetFieldAIDto {
    char mark;
    public SetFieldAIDto() {}
    public SetFieldAIDto(char mark) {
        this.mark = mark;
    }
    public char getMark() {
        return mark;
    }
}