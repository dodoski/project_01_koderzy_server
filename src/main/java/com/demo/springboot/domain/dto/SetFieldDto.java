package com.demo.springboot.domain.dto;

public class SetFieldDto {
    private int fieldX;
    private int fieldY;
    private char mark;
    public SetFieldDto() {
    }
    public SetFieldDto(int fieldX, int fieldY, char mark) {
        this.fieldX = fieldX;
        this.fieldY = fieldY;
        this.mark = mark;
    }
    public int getFieldX() {
        return fieldX;
    }
    public int getFieldY() {
        return fieldY;
    }
    public char getMark() {
        return mark;
    }
}
