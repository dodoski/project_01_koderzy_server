package com.demo.springboot.domain.dto;

import java.util.Vector;

public class TicTacToeBoard {
    public Vector<Vector<String>>  matrix;
    public TicTacToeBoard(Vector<Vector<String>> matrix) {
        this.matrix = matrix;
    }
    public Vector<Vector<String>> getMatrix() {
        return matrix;
    }
    public void setMatrix(Vector<Vector<String>> matrix) {
        this.matrix = matrix;
    }
    @Override
    public String toString() {
        return matrix.get(0).get(0) + ", " + matrix.get(0).get(1) + ", " + matrix.get(0).get(2) + ", " + System.lineSeparator()
                + matrix.get(1).get(0) + ", " + matrix.get(1).get(1) + ", " + matrix.get(1).get(2) + ", "+ System.lineSeparator()
                + matrix.get(0).get(2) + ", " + matrix.get(0).get(2) + ", " + matrix.get(2).get(2) + ", ";
    }
}
