package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.SetFieldAIDto;
import com.demo.springboot.domain.dto.SetFieldDto;
import com.demo.springboot.domain.dto.TicTacToeBoard;
import com.demo.springboot.service.impl.TicTacToe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/api")
public class TicTacToeApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TicTacToeApiController.class);

    @Autowired
    TicTacToe ticTacToe;

    @CrossOrigin
    @GetMapping(value = "/tictactoe")
    public ResponseEntity<Map<String, String>> serverTest() {
        LOGGER.info("--- run server status");

        Map<String, String> serverTestMessage = new HashMap<>();
        serverTestMessage.put("server-status", "RUN :-)");

        return new ResponseEntity<>(serverTestMessage, HttpStatus.OK);
    }
    @RequestMapping(value = "/tictactoe/result", method = RequestMethod.GET)
    public ResponseEntity<TicTacToeBoard> result() {
        TicTacToeBoard ticTakToeBoard = new TicTacToeBoard(ticTacToe.getVector());
        LOGGER.info("Zwraca aktualny stan planszy (dane potrzebne klientowi do aktualizacji planszy).");

        return new ResponseEntity<>(ticTakToeBoard, HttpStatus.OK);
    }
    @RequestMapping(value = "/tictactoe/reset-game", method = RequestMethod.POST)
    public ResponseEntity<Void> resetGame() {
        LOGGER.info("Przywraca grę do stanu początkowego.");
        ticTacToe.resetGame();
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "/tictactoe/set-field-by-user", method = RequestMethod.POST)
    public ResponseEntity<String> setFieldByUser(@RequestBody SetFieldDto setFieldDto) {
        LOGGER.info("Wprowadza określony znak w podane współrzędne pola gry.");
        LOGGER.info("### X: {}, Y: {}, Znak: {} ", setFieldDto.getFieldX(), setFieldDto.getFieldY(), setFieldDto.getMark());
        if (setFieldDto.getFieldX() > 2 || setFieldDto.getFieldY() > 2)
            return new ResponseEntity<>("Wrong position used!", HttpStatus.BAD_REQUEST);
        else if(ticTacToe.getStringAt(setFieldDto.getFieldX(),setFieldDto.getFieldY()) == null || ticTacToe.getStringAt(setFieldDto.getFieldX(), setFieldDto.getFieldY()).equals(" ")){
            ticTacToe.setPlayerSym(String.valueOf(setFieldDto.getMark()));
            ticTacToe.setField(setFieldDto.getFieldX(), setFieldDto.getFieldY(), String.valueOf(setFieldDto.getMark()));
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        else{
            return new ResponseEntity<>("Position you want to choose is already marked!", HttpStatus.BAD_REQUEST);
        }
    }
    @RequestMapping(value = "/tictactoe/set-field-by-ai", method = RequestMethod.POST)
    public ResponseEntity<Void> setFieldByAI(@RequestBody SetFieldAIDto setFieldAIDto) throws InterruptedException {
        LOGGER.info("Wprowadza określony znak w pole które wyznaczy algorytm AI.");
        LOGGER.info("### Znak: {} ", setFieldAIDto.getMark());
        if (!String.valueOf(setFieldAIDto.getMark()).equals(ticTacToe.getPlayerSym())){
            if(ticTacToe.setFieldAi(String.valueOf(setFieldAIDto.getMark())))
                return new ResponseEntity<>(HttpStatus.CREATED);
            else
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
