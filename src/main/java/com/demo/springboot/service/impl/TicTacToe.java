package com.demo.springboot.service.impl;

import com.demo.springboot.service.TicTacToeService;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Random;
import java.util.Vector;

@Service
public class TicTacToe  implements TicTacToeService {
    static final private int sizeBoard = 3;
    private String[][] board = new String[sizeBoard][sizeBoard];
    String playerSym;

    public void setPlayerSym(String playerSym) {
        this.playerSym = playerSym;
    }
    public TicTacToe() {
        eraseBoard();
    }
    public String getPlayerSym() {
        return playerSym;
    }
    public void resetGame(){
        eraseBoard();
    }
    private void eraseBoard(){
        for(int i = 0; i < sizeBoard; i++){
            for(int j = 0; j < sizeBoard ; j++){
                board[i][j] = " ";
            }
        }
    }
    public void setField(int fieldX, int fieldY, String mark){
        board[fieldX][fieldY] = mark;
    }
    public String getStringAt(int x, int y){
        return board[x][y];
    }
    @Override
    public String toString(){
        return board[0][0]+", "+ board[0][1]+", "+board[0][2]+", "+ System.getProperty("line.separator") +
                board[1][0]+", "+board[1][1]+", "+board[1][2]+", "+ System.getProperty("line.separator") +
                board[2][0]+", "+board[2][1]+", "+board[2][2];
    }
    public boolean setFieldAi(String mark){
        Random gen = new Random();
        int i = 0;
        int aiX = gen.nextInt(3);
        int aiY = gen.nextInt(3);
        while(!Objects.equals(board[aiX][aiY], " ")){
            aiX = gen.nextInt(3);
            aiY = gen.nextInt(3);
            i++;
            if(i == 20){
                return false;
            }
        }
        board[aiX][aiY] = mark;
        return true;
    }
    public Vector<Vector<String>> getVector() {
        Vector<Vector<String>>  matrix= new Vector<Vector<String>>();
        for(int i=0;i<3;i++){
            Vector<String> r = new Vector<>();
            for(int j=0;j<3;j++){
                r.add(String.valueOf(board[i][j]));
            }
            matrix.add(r);
        }
        return matrix;
    }
}
